Ski station
===========

Author: Tao "DePierre" Sauvage
Creation: 12/3/2012

This project is provided by the UTBM for students following lecture AG44 (Graph Theory)

The set of rules is available into the PDF file. We have to manage files which
contain a ski station representation.

Our goal is to provide algorithms to answer such problem.

The Python language is used here and the project is under WTFPL license.
