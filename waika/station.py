"""

        Ski station representation.

"""


import sys
import datetime
from igraph import Graph
from igraph import plot
from igraph.drawing import colors
from waika.spot import SkiSpot
from waika.route import Route


INF = 1234567891234567891234567890.
NULL = -42


class SkiStation(object):
    """Ski station representation.

    Contains a list of a spot in the station and a list of the routes in the
    station.

    """

    def __init__(self, spots = [], routes = [], restrictions = []):
        """self-explanatory"""

        self.spots = spots
        self.routes = routes
        self.restricted_routes = routes
        self.restrictions = restrictions
        self.spot_start = -1
        self.spot_end = -1
        self.shortest_path = []
        self.possible_dest = []
        self.path = [[INF for i in range(len(self.spots))]\
                for j in range(len(self.spots))]
        self.succ = [[NULL for i in range(len(self.spots))]\
                for j in range(len(self.spots))]
        self._floyd_warshall()

    def spots_to_vertices(self, spots):
        """self-explanatory"""

        vertices = []
        for spot in spots:
            vertices.append(spot.get_id())
        return vertices

    def routes_to_edges(self, routes):
        """self-explanatory"""

        edges = []
        for i in range(len(routes)):
            edges.append((routes[i].get_start(), routes[i].get_end()))
        return edges

    def get_neighbours(self):
        """self-explanatory"""

        list_vertices = self.spots_to_vertices(self.spots)
        for vertex in list_vertices:
            list_neighbours = []
            for edge in self.edges:
                if edge[0] == vertex:
                    list_neighbours.append(edge[1])
            self.vertices.update({vertex: list_neighbours})

    def get_weight(self):
        """self-explanatory"""

        weights = []
        for route in self.routes:
            weights.append(self.compute_speed(\
                    abs(self.spots[route.get_start()].get_altitude() -\
                        self.spots[route.get_end()].get_altitude()),\
                        route.get_speed(),\
                        route.get_label()))
        return weights

    def add_spots(self, spots = []):
        """self-explanatory"""

        self.spots.extend(spots)
        self.__init_path()

    def add_routes(self, routes = []):
        """self-explanatory"""

        self.routes.extend(routes)

    def get_spot(self, id_spot):
        """Get the spot from id_spot"""

        for i in range(len(self.spots)):
            if self.spots[i].get_id() == id_spot:
                return i
        return -1

    def get_edge(self, id_spot1, id_spot2):
        """self-explanatory"""

        for i in range(len(self.edges)):
            if self.edges[i][0] == id_spot1\
                    and self.edges[i][1] == id_spot2:
                return i
        return -1

    def get_route(self, id_spot1, id_spot2):
        """Get the route from id_spot1 to id_spot2."""

        for i in range(len(self.routes)):
            if self.routes[i].get_start() == id_spot1\
                    and self.routes[i].get_end() == id_spot2:
                return i
        return -1

    def compute_speed(self, altitude, speed, label):
        """Compute the speed in m/mn."""

        if not speed[0]:
            return altitude * (1. * speed[1] / 100)
        else:
            if speed[1].has_key('f'):
                fixed_speed = speed[1]['f']
                return fixed_speed + altitude * (1. * speed[1]['a'] / 100)
            else:
                if 'navette' in label:
                    label = label.strip('navette')
                    return altitude * speed[1][label]

    def set_restrictions(self, restrictions = []):
        """Set the restrictions for the computation of the shortest paths."""

        self.restrictions = restrictions
        unrestricted_routes = []
        for i in range(len(self.routes)):
            if self.routes[i].get_transport() in restrictions:
                unrestricted_routes.append(self.routes[i])
            elif self.routes[i].get_transport() in\
                    ['TPH', 'TC', 'TSD', 'TS', 'TK', 'BUS']:
                unrestricted_routes.append(self.routes[i])
        self.routes = unrestricted_routes
        self.vertices = {}
        self.edges = self.routes_to_edges(self.routes)
        self.weights = self.get_weight()
        self.get_neighbours()

    def _floyd_warshall(self):
        """Apply Floyd-Wharshall algirithm to compute the shortest paths
        between all the ski spots.

        """

        for i in range(len(self.spots)):
            for j in range(len(self.spots)):
                id_route = self.get_route(i, j)
                # No route between i and j
                if id_route == -1:
                    self.path[i][j] = INF
                else:
                    route = self.routes[id_route]
                    self.path[i][j] = self.compute_speed(\
                            abs(self.spots[i].get_altitude() -\
                                self.spots[j].get_altitude()),\
                                route.get_speed(),\
                                route.get_label())

        # Computation of the shortest paths
        for k in range(len(self.spots)):
            for i in range(len(self.spots)):
                for j in range(len(self.spots)):
                    if self.path[i][k] + self.path[k][j] < self.path[i][j]:
                        self.path[i][j] = self.path[i][k] + self.path[k][j]
                        self.succ[i][j] = k

    def get_shortest_path(self, id_spot1, id_spot2):
        """Get the shortest route from id_spot1 and id_spot2."""

        def recursive_path(id_spot1, id_spot2):
            """Recursive algorithm to find the shortest path."""

            if self.path[id_spot1][id_spot2] == INF:
                return 'No path'
            inter = self.succ[id_spot1][id_spot2]
            if inter == NULL:
                return ' '
            else:
                return str(self.get_shortest_path(id_spot1, inter)) +\
                        str(inter) +\
                        str(self.get_shortest_path(inter, id_spot2))

        raw_shortest_path = recursive_path(id_spot1, id_spot2)
        self.spot_start = id_spot1
        self.spot_end = id_spot2
        self.shortest_path = raw_shortest_path

        return self.shortest_path

    def dfs(self, start):
        """Apply DFS algorithm to find what spots the user can reach from a
        start one.

        """

        self.spots[self.get_spot(start)].set_explored()
        stack = []
        stack.extend(self.vertices[start])
        while stack:
            neighbour = stack.pop()
            for succ in self.vertices[neighbour]:
                if not self.spots[self.get_spot(succ)].get_explored():
                    self.spots[self.get_spot(succ)].set_explored()
                    stack.append(succ)

        possible_dest = []
        console_dest = []
        for spot in self.spots:
            if spot.get_explored():
                label = spot.get_label()
                id_spot = spot.get_id()
                console_dest.append(label + '-' + str(id_spot))
                possible_dest.append(id_spot)
        self.possible_dest = possible_dest
        return console_dest

    def display_c(self, id_start, id_end, restrictions = []):
        """Display in console mode the station."""

        if not restrictions:
            self.get_shortest_path(id_start, id_end)
            print 'Shortest path between', self.spot_start,\
                    'and', str(self.spot_end) + ':'
            print str(self.spot_start) + self.shortest_path + str(self.spot_end)
            print
            id_spots = [id_start]
            temp = self.shortest_path.split()
            id_spots.extend([int(i) for i in temp])
            id_spots.append(id_end)
            edges = {}
            total_speed = 0
            for i in range(len(id_spots) - 1):
                id_route = self.get_route(id_spots[i], id_spots[i + 1])
                label = self.routes[id_route].get_label()
                transport = self.routes[id_route].get_transport()
                speed = self.compute_speed(\
                            abs(self.spots[id_spots[i]].get_altitude() -\
                                self.spots[id_spots[i + 1]].get_altitude()),\
                                self.routes[id_route].get_speed(),\
                                self.routes[id_route].get_label())
                total_speed = total_speed + speed
                edges.update({id_route: [label,\
                        transport,\
                        str(datetime.timedelta(minutes = speed))]})
            print 'Follow those routes:'
            for id_edge in edges:
                print edges[id_edge][0] + ':', edges[id_edge][1], 'for',\
                        edges[id_edge][2]
            print
            print 'Total:', str(datetime.timedelta(minutes = total_speed))
        else:
            self.set_restrictions(restrictions)
            self.dfs(id_start)
            print 'From', self.spots[id_start].get_label(),\
                    '-', self.spots[id_start].get_id()
            print 'With the following restrictions:', self.restrictions
            print
            print 'The reachable spots are:'
            for id_spot in self.possible_dest:
                print 'Destination:', self.spots[id_spot].get_label(),\
                        '-', self.spots[id_spot].get_id()

    def display_g(self):
        """Display in graphical mode the station."""

        # Force computation if it's not already done
        # Creation of a directed graph with as much vertices that the GameGraph
        g_display = Graph(len(self.spots), directed = True)
        # Creation of a sorted list of the vertices and the edges of the graph
        sorted_vertices = []
        v_label = []
        v_colors = []
        sorted_edges = []
        e_colors = []
        e_size = []

        shortest_path = []
        if not 'No path' in self.shortest_path:
            if self.spot_end != -1:
                temp = self.shortest_path.split()
                shortest_path.append(self.spot_start)
                shortest_path.extend([int(el) for el in temp])
                shortest_path.append(self.spot_end)
        id_shortest_path = []
        for i in range(len(shortest_path) - 1):
            id_shortest_path.append(self.get_route(shortest_path[i],\
                    shortest_path[i + 1]))

        for v in self.spots:
            sorted_vertices.append(v.get_id())
            v_label.append(v.get_label() + '-' + str(v.get_id()))
        for e in self.routes:
            sorted_edges.append((e.get_start(), e.get_end()))
            if e.get_transport() == "N":
                e_colors.append('black')
            elif e.get_transport() == "B":
                e_colors.append('blue')
            elif e.get_transport() == "R":
                e_colors.append('red')
            elif e.get_transport() == "V":
                e_colors.append('green')
            else:
                e_colors.append('grey')
            if not self.restrictions:
                if not e.get_id() in id_shortest_path:
                    e_size.append(1)
                else:
                    e_size.append(4)
            else:
                e_size.append(1)

        for v in self.spots:
            if v.get_id() in self.possible_dest:
                v_colors.append("gold")
            else:
                v_colors.append("silver")

        g_display.add_edges(sorted_edges)

        visual_style = {}
        visual_style["vertex_label"] = v_label
        visual_style["layout"] = g_display.layout("kk")
        visual_style["vertex_color"] = v_colors
        visual_style["edge_color"] = e_colors
        visual_style["edge_width"] = e_size
        visual_style["vertex_shape"] = ['circle']
        visual_style["bbox"] = (1024, 768)
        visual_style["margin"] = 50

        # Graphical draw of the graph
        plot(g_display, **visual_style)


