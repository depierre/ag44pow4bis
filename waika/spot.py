"""

        Ski station represention.

"""


class SkiSpot(object):
    """Represents a ski spot of the ski station.

    Contains the id of the vertex, the name and the altitude.

    """

    def __init__(self, id_spot = -1, label = "", altitude = -1):
        """self-explanatory"""

        self.id_spot = id_spot
        self.label = label
        self.altitude = altitude
        self.explored = False

    def set_explored(self, explored = True):
        """self-explanatory"""

        self.explored = explored

    def get_explored(self):
        """self-explanatory"""

        return self.explored

    def get_id(self):
        """Get the id of the ski spot."""

        return self.id_spot

    def get_altitude(self):
        """Get the altitude of the ski spot."""

        return self.altitude

    def get_label(self):
        """Get the label of the ski spot."""

        return self.label

    def display_c(self):
        """Display in console mode the spot."""

        print 'ID:', str(self.id_spot)
        print 'Label:', self.label
        print 'Altitude:', str(self.altitude)
