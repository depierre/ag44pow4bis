"""

        Parse the datafile which contains the representation of a ski station.

"""


import os
import sys
from waika.spot import SkiSpot
from waika.route import Route
from waika.station import SkiStation


def parse(args):
    """self-explanatory"""

    if len(args) > 1 and not os.path.isfile(args[0]):
        print 'Error: file not found.'
        sys.exit(1)
    else:
        datafile = open(args[0], 'r')
        data = datafile.readlines()
        spots = []
        number_spots = int(data[0])
        for i in range(1, number_spots + 1):
            params = data[i].split()
            if params[1].isdigit():
                params[1] = spots[-1].get_label()
            spots.append(SkiSpot(int(params[0]) - 1,\
                    params[1], int(params[2])))
        routes = []
        for i in range(number_spots + 2, len(data), 1):
            params = data[i].split()
            routes.append(Route(int(params[0]) - 1, params[1], params[2], \
                    int(params[3]) - 1, int(params[4]) - 1))
        ski_station = SkiStation(spots, routes)
        if len(args) >= 2:
            args[1] = int(args[1])
            if len(args) >= 3 and args[2].isdigit():
                args[2] = int(args[2])
                ski_station.display_c(args[1], args[2])
                ski_station.display_g()
            else:
                if len(args) >= 3:
                    ski_station.display_c(args[1], -1, args[2:])
                    ski_station.display_g()
        return ski_station
