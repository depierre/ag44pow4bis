"""

        Route representation in the ski station.

"""


from waika.transport import Transport


class Route(object):
    """Representation of a route in the ski station.

    Contains an id, a label, a transport, a start point and an end point.

    """

    def __init__(self, id_route = -1, label = "", transport = "", \
            start = -1, end = -1):
        """self-explanatory"""

        self.id_route = id_route
        self.label = label
        self.transport = Transport(transport)
        self.start = start
        self.end = end
        self.explored = False

    def set_explored(self, explored = True):
        """self-explanatory"""

        self.explored = explored

    def get_explored(self):
        """self-explanatory"""

        return self.explored

    def get_start(self):
        """Get the start spot."""

        return self.start

    def get_end(self):
        """Get the end spot."""

        return self.end

    def get_label(self):
        """Get the label of the route."""

        return self.label

    def get_id(self):
        """Get the id of the route."""

        return self.id_route

    def get_speed(self):
        """Get the speed of the route."""

        return self.transport.get_speed()

    def get_transport(self):
        """Get the label of the transport of the route."""

        return self.transport.get_label()

    def display_c(self):
        """Display in console mode the route."""

        print 'ID:', str(self.id_route)
        print 'Label:', str(self.label)
        print 'Transport', self.transport.get_label()
        print 'Start', str(self.start), 'End', str(self.end)
