"""

        Representation of the transports available in the ski station.

"""


SPEED_T = {'V': 5,\
        'B': 4,\
        'R': 3,\
        'N': 2,\
        'KL': 0.1,\
        'SURF': 10}


C_SPEED_T = {'TPH': {'f': 4, 'a': 2},\
        'TC': {'f': 2, 'a': 3},\
        'TSD': {'f': 1, 'a': 3},\
        'TS': {'f': 1, 'a': 4},\
        'TK': {'f': 1, 'a': 4},\
        'BUS': {'2000-1600': 40, '1600-2000':40,\
            '1600-1800': 30, '1800-1600': 30}}


class Transport(object):
    """Transport representation.

    Contains a label and a speed move.

    """

    def __init__(self, label = ""):
        """self-explanatory"""

        self.label = label
        self.speed = -1
        self.c_speed = False
        if label in SPEED_T:
            self.speed = SPEED_T[label]
        else:
            self.speed = C_SPEED_T[label]
            self.c_speed = True

    def get_speed(self):
        """Get the speed of the transport"""

        return [self.c_speed, self.speed]

    def get_label(self):
        """Get the label of the transport"""

        return self.label
