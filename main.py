#!/usr/bin/env python2


"""

        This is the main python file of the AG44 project. It is the entry
        point of the project. It is only intended to call on the CLI interface,
        which will trigger everything itself.

"""


import sys
from waika.parse import parse


if __name__ == '__main__':
    if len(sys.argv) >= 1:
        parse(sys.argv[1:])
